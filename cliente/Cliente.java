package cliente;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente implements Runnable{
	
	 private Socket cliente;	 
	 PrintStream saida;
	 Scanner entrada;
	 
	 Scanner leitor = new Scanner(System.in); 

	    public Cliente(Socket cliente){
	    try{
	        entrada = new Scanner(cliente.getInputStream());	   
			saida = new PrintStream(cliente.getOutputStream());
	    }catch(Exception e){
	    	
	    }
	    }

	
	
	
	public void run(){
		try {
			
			System.out.println("O cliente conectou ao servidor");		
		
			
			String texto;
			String enviarParaServidor;  
			while((texto = entrada.nextLine()) != null){
				System.out.println(texto);
				          
				  
				
				if(texto.equals("Qual o seu nome")){
				enviarParaServidor=leitor.nextLine();  
		        saida.println(enviarParaServidor);    	        
		             		        	      
				}
				
				if(texto.equals("Digite o numero da sala.")){
					enviarParaServidor=leitor.nextLine();  
			        saida.println(enviarParaServidor);   		 	        
			             		        	      
					}			
							
				if(texto.equals("Digite '/iniciar' para comecar ou '/listar' para listar os jogadores da sua sala")){
				
					
					enviarParaServidor=leitor.nextLine();  
			        saida.println(enviarParaServidor);  
			             
		 	        
			             		        	      
					}
				
				
				
				if(texto.equals("Quantos palitos vai jogar")){
					enviarParaServidor=leitor.nextLine();  
			        saida.println(enviarParaServidor);     
		 	        
			             		        	      
					}
				
				if(texto.equals("Digite seu Palpite")){
					enviarParaServidor=leitor.nextLine();  
			        saida.println(enviarParaServidor);     
		 	        
			             		        	      
					}
				
				
			
			}
			
			
	
			saida.close();
			entrada.close();
			this.cliente.close();
			System.out.println("Fim da thread...");
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		
		Socket cliente =  new Socket("localhost", 9090);
		
		Cliente c = new Cliente(cliente);
		Thread t = new Thread(c);
		t.start();
	}

}
