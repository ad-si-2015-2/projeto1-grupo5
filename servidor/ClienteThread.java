package servidor;



import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.net.Socket;

public class ClienteThread implements Runnable{
		private Jogador jogador;
		private Socket conexao;
		private BufferedReader doCliente;
		private DataOutputStream paraCliente;
	
		public ClienteThread (Socket conexao){
			this.conexao = conexao;
			
		}

	public Jogador getJogador() {
			return jogador;
		}



		public void setJogador(Jogador jogador) {
			this.jogador = jogador;
		}



		public Socket getConexao() {
			return conexao;
		}



		public void setConexao(Socket conexao) {
			this.conexao = conexao;
		}



		public BufferedReader getDoCliente() {
			return doCliente;
		}



		public void setDoCliente(BufferedReader doCliente) {
			this.doCliente = doCliente;
		}



		public DataOutputStream getParaCliente() {
			return paraCliente;
		}



		public void setParaCliente(DataOutputStream paraCliente) {
			this.paraCliente = paraCliente;
		}



	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
